import typing

import hikari
import hikari.utilities.undefined

from .structs import Result, User, result_to_event_data

UNDEFINED = hikari.utilities.undefined.UNDEFINED


def get_event_info(event: hikari.events.base_events.Event) -> Result:
    user = getattr(event, 'user', None)
    user_res: typing.Optional[User] = None

    if isinstance(event, hikari.events.guild_events.BanEvent):
        user = None

    if (user is not None and user != UNDEFINED and
        UNDEFINED not in (
                    getattr(user, 'id', UNDEFINED),
                    getattr(user, 'username', UNDEFINED),
                    getattr(user, 'discriminator', UNDEFINED)
            )):
        user_res = User(
            username=user.username,
            discriminator=user.discriminator,
            id=user.id
        )

    user_id = getattr(event, 'user_id', None)

    if isinstance(event, hikari.events.guild_events.BanEvent):
        user_id = None

    if user_res is None and user_id is not None:
        user_res = User(id=user_id)

    message = getattr(event, 'message', None)
    if user_res is None and message is not None:
        user = getattr(message, 'author', None)
        if user is not None and user != UNDEFINED and UNDEFINED not in (
                        getattr(user, 'id', UNDEFINED),
                        getattr(user, 'username', UNDEFINED),
                        getattr(user, 'discriminator', UNDEFINED)
                ):
            user_res = User(
                username=user.username,
                discriminator=user.discriminator,
                id=user.id
            )
        elif user is not None and user != UNDEFINED:
            user_res = User(
                id=getattr(user, 'id', 1337)
            )

    return Result(
        user=user_res,
        guild_id=getattr(event, 'guild_id', None),
        channel_id=getattr(event, 'channel_id', None)
    )


__all__ = ('result_to_event_data', 'get_event_info')
