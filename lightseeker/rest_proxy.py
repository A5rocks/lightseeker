import typing

import hikari
import hikari.api.rest
import hikari.impl.rest

from .breadcrumbs import add_breadcrumb

T = typing.TypeVar('T')


def _request_breadcrumb(
        args: typing.List[typing.Any],
        kwargs: typing.Dict[str, typing.Any],
        function_name: str,
        response: typing.Any
) -> None:
    errored = isinstance(response, str) and response.startswith('Errored: ')
    level: typing.Union[typing.Literal['info'], typing.Literal['warning']] = 'warning' if errored else 'info'

    add_breadcrumb(
        category='hikari.impl.rest',
        data={
            'arguments': args,
            'keyword arguments': kwargs,
            'function name': function_name,
            'response': response,
            'errored': str(errored)
        },
        level=level,
        type='http'
    )


def proxy_for(coro: typing.Callable[..., typing.Coroutine[typing.Any, typing.Any, T]]
              ) -> typing.Callable[..., typing.Coroutine[typing.Any, typing.Any, T]]:
    async def proxied_coro(*args: typing.Any, **kwargs: typing.Any) -> T:
        try:
            res = await coro(*args, **kwargs)
            _request_breadcrumb(list(args), kwargs, getattr(coro, '__name__', 'Not found.'), res)
            return res
        except hikari.errors.HTTPError as e:
            _request_breadcrumb(list(args), kwargs, getattr(coro, '__name__', 'Not found.'), f'Errored: {e!r}')

            raise e

    return proxied_coro


PROXIED_FUNCTIONS = dir(hikari.api.rest.IRESTClient)
PROXIED_FUNCTIONS.remove('fetch_gateway_bot')


# this could be made generic, but there's no need.
class RestProxy:
    __slots__ = '_old'

    _old: hikari.impl.rest.RESTClientImpl

    def __init__(self, old: hikari.impl.rest.RESTClientImpl) -> None:
        self._old = old

    def __getattr__(self, item: str) -> typing.Any:
        res = getattr(self._old, item)

        if not item.startswith('_') and item in PROXIED_FUNCTIONS:
            res = proxy_for(res)

        return res
