# type: ignore
from setuptools import setup

setup(
    name='lightseeker',
    version='0.1.0',
    author='A5rocks',
    author_email='5684371-A5rocks@users.noreply.gitlab.com',
    package_data={'lightseeker': ['py.typed']},
    packages=['lightseeker'],
    license='BSD-3-Clause',
    python_requires='>=3.8.0,<3.10',
    description='Sentry integration with your hikari bot.',
    install_requires=[
        'hikari~=2.0.0.dev52',
        'sentry-sdk~=0.16.2'
    ],
    zip_safe=False
)
