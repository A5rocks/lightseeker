import contextvars
import time
import typing

from .errors import BreadcrumbsError

breadcrumbs: contextvars.ContextVar[
    typing.List[typing.Dict[str, typing.Any]]
] = contextvars.ContextVar('breadcrumbs', default=[])

BREADCRUMB_LEVEL = typing.Union[
    typing.Literal['fatal'],
    typing.Literal['error'],
    typing.Literal['warning'],
    typing.Literal['info'],
    typing.Literal['debug']
]


def add_breadcrumb(
        category: str = '',
        data: typing.Optional[typing.Dict[str, typing.Any]] = None,
        level: BREADCRUMB_LEVEL = 'info',
        message: typing.Optional[str] = None,
        type: str = 'default'
) -> None:
    if data is None and message is None:
        raise BreadcrumbsError('You have to supply either `data`, `message`, or both.')

    breadcrumb = {'level': level, 'category': category, 'type': type, 'timestamp': time.time()}

    if message is not None:
        breadcrumb['message'] = message

    if data is not None:
        breadcrumb['data'] = data

    breadcrumbs.set(breadcrumbs.get() + [breadcrumb])
