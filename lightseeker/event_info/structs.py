import typing

import attr


@attr.s(auto_attribs=True, slots=True)
class User:
    id: int
    username: str = 'Not found.'
    discriminator: str = 'Not Found'


@attr.s(auto_attribs=True, slots=True)
class Result:
    user: typing.Optional[User] = None
    guild_id: typing.Optional[int] = None
    channel_id: typing.Optional[int] = None


def result_to_event_data(result: Result) -> typing.Dict[str, typing.Any]:
    res: typing.Dict[str, typing.Any] = {}

    if result.user is not None:
        res['user'] = attr.asdict(result.user)

    if result.channel_id is not None:
        if res.get('extra') is None:
            res['extra'] = {}

        res['extra']['channel_id'] = result.channel_id

    if result.guild_id is not None:
        if res.get('extra') is None:
            res['extra'] = {}

        res['extra']['guild_id'] = result.guild_id

    return res
