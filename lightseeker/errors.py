from hikari.errors import HikariError


class LightseekerError(HikariError):
    pass


class BreadcrumbsError(LightseekerError):
    pass
