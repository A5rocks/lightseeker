import typing

import hikari
import sentry_sdk
from sentry_sdk.utils import event_from_exception, exc_info_from_error

from .breadcrumbs import add_breadcrumb, breadcrumbs
from .event_info import get_event_info, result_to_event_data
from .rest_proxy import RestProxy


def report_exception(
        e: BaseException,
        event_data: typing.Optional[typing.Dict[str, typing.Any]] = None,
        context: typing.Optional[typing.Dict[str, typing.Any]] = None) -> None:
    event_data = event_data or {}
    event_data = event_data.copy()

    if context is not None:
        if event_data.get('extra') is None:
            event_data['extra'] = {}

        event_data['extra'].update(context)

    event_data_, hint = event_from_exception(exc_info_from_error(e))
    event_data.update(event_data_)
    event_data['breadcrumbs'] = breadcrumbs.get()

    sentry_sdk.api.capture_event(event_data, hint)


def prepare_bot(bot: hikari.Bot) -> hikari.Bot:
    @bot.listen(hikari.events.base_events.ExceptionEvent)
    async def seeker(event: hikari.events.base_events.ExceptionEvent[hikari.events.base_events.FailedEventT]) -> None:
        if event.exception.__traceback__ is not None:
            ne = event.exception.with_traceback(event.exception.__traceback__.tb_next)
        else:
            # should be so few cases it doesn't matter...
            ne = event.exception

        event_data = result_to_event_data(get_event_info(event.failed_event))
        report_exception(ne, event_data)

    bot._rest = RestProxy(bot.rest)  # type: ignore

    return bot


__all__ = ('prepare_bot', 'add_breadcrumb', 'get_event_info', 'result_to_event_data', 'report_exception')
