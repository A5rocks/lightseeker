### lightseeker

A library which ties together your *hikari* bot and [*Sentry*](https://sentry.io).

Install:
```sh
python -m pip install git+https://gitlab.com/A5rocks/lightseeker.git@v0.1.0
```

Usage:
```python
import hikari
import sentry_sdk
from lightseeker import prepare_bot

...

bot = hikari.Bot(...)
prepare_bot(bot)

...

# `default_integrations` is set to `False` because the `logging` extension
# gets used up otherwise... so you'll have two errors. However, if you want
# logging breadcrumbs you can disable sending `logging` events... check out:
# https://docs.sentry.io/platforms/python/logging/
sentry_sdk.init(..., default_integrations=False)
```


For hikari extension developers:
 * `add_breadcrumb(data={'foo': 'bar'})` adds breadcrumbs to the context (so
   that they will only be sent if an exception is to be sent to Sentry in this
   context).
 * `report_exception` reports an exception to *Sentry*. Note that you can pass
   in a second argument, which should be the raw event data (so you can set
   tags and such), while the third argument is context data such as
   `channel_id`, `guild_id`, etc.)
 * speaking of raw event data, give `get_event_info` an event to let
   *lightseeker* turn the event into a `Result`, which you can then apply
   `result_to_event_data` to.
